const should = require('chai').should()
const mongoUriSafeLog = require('../index')

const uriUPArray = [
  'mongodb://service-account:super-secret-password@db.somedomain.com:27017/collection?readPreference=primary&authSource=admin',
  'mongodb://service-account:super-secret-password@db.somedomain.com:27017/collection?readPreference=primary',
  'mongodb://service-account:super-secret-password@db.somedomain.com:27017/collection',
  'mongodb://service-account:super-secret-password@db1.somedomain.com:27017,db2.somedomain.com:27017,db3.somedomain.com:27017/admin?replicaSet=replica-name&readPreference=primary'
]

const uriUArray = [
  'mongodb://service-account@db.somedomain.com:27017/collection?readPreference=primary&authSource=admin',
  'mongodb://service-account@db.somedomain.com:27017/collection?readPreference=primary',
  'mongodb://service-account@db.somedomain.com:27017/collection',
  'mongodb://service-account@db1.somedomain.com:27017,db2.somedomain.com:27017,db3.somedomain.com:27017/admin?replicaSet=replica-name&readPreference=primary'
]

const uriArray = [
  'mongodb://db.somedomain.com:27017/collection?readPreference=primary&authSource=admin',
  'mongodb://db.somedomain.com:27017/collection?readPreference=primary',
  'mongodb://db.somedomain.com:27017/collection',
  'mongodb://db1.somedomain.com:27017,db2.somedomain.com:27017,db3.somedomain.com:27017/admin?replicaSet=replica-name&readPreference=primary'
]

describe('Test URIs with default settings', () => {
  it('tests a URI with a username and password', () => {
    const cleanedArray = uriUPArray.map(uri => mongoUriSafeLog(uri))

    cleanedArray.forEach((uri, idx) => {
      uri.should.equal(uriUPArray[idx].replace('super-secret-password', 'PASSWORD'))
    })
  })
  it('tests a URI with just a username', () => {
    const cleanedArray = uriUArray.map(uri => mongoUriSafeLog(uri))

    cleanedArray.forEach((uri, idx) => {
      uri.should.equal(uriUArray[idx])
    })
  })
  it('tests a URI with no username and no password', () => {
    const cleanedArray = uriArray.map(uri => mongoUriSafeLog(uri))

    cleanedArray.forEach((uri, idx) => {
      uri.should.equal(uriArray[idx])
    })
  })
})

describe('Test URIs with username supression settings', () => {
  it('tests a URI with a username and password', () => {
    const cleanedArray = uriUPArray.map(uri => mongoUriSafeLog(uri, true))

    cleanedArray.forEach((uri, idx) => {
      let expected = uriUPArray[idx].replace('super-secret-password', 'PASSWORD')

      expected = expected.replace('service-account', 'USERNAME')
      uri.should.equal(expected)
    })
  })
  it('tests a URI with just a username', () => {
    const cleanedArray = uriUArray.map(uri => mongoUriSafeLog(uri, true))

    cleanedArray.forEach((uri, idx) => {
      uri.should.equal(uriUArray[idx].replace('service-account', 'USERNAME'))
    })
  })
  it('tests a URI with no username and no password', () => {
    const cleanedArray = uriArray.map(uri => mongoUriSafeLog(uri, true))

    cleanedArray.forEach((uri, idx) => {
      uriArray[idx].should.equal(uri)
    })
  })
})

describe('Test URIs with password supression settings off', () => {
  it('tests a URI with a username and password', () => {
    const cleanedArray = uriUPArray.map(uri => mongoUriSafeLog(uri, false, true))

    cleanedArray.forEach((uri, idx) => {
      let expected = uriUPArray[idx].replace('super-secret-password', 'PASSWORD')

      uri.should.equal(expected)
    })
  })
  it('tests a URI with just a username', () => {
    const cleanedArray = uriUArray.map(uri => mongoUriSafeLog(uri, false, true))

    cleanedArray.forEach((uri, idx) => {
      uri.should.equal(uriUArray[idx].replace('super-secret-password', 'PASSWORD'))
    })
  })
  it('tests a URI with no username and no password', () => {
    const cleanedArray = uriArray.map(uri => mongoUriSafeLog(uri, false, true))

    cleanedArray.forEach((uri, idx) => {
      uriArray[idx].should.equal(uri)
    })
  })
})

describe('Test Malformed URI', () => {
  it('craps out on a bad URI', () => {
    let result

    try {
      result = mongoUriSafeLog('domainName.com')
    } catch (err) {
      result = err
    }
    result.message.should.equal('Malformed Mongo URI')
  })
  it('craps out on a bad URI with user and pass', () => {
    let result

    try {
      result = mongoUriSafeLog('mongob://user:password@this.com')
    } catch (err) {
      result = err
    }
    result.should.not.be.a('string')
    result.message.should.equal('Malformed Mongo URI')
  })
})
