const mongoUriSafeLog = (uri, supressUsername = false, supressPassword = true) => {
  const re = /(mongodb:\/\/)((?<user>.*\:)(?<pass>.*@)|(?<user2>.*)@)?(?<url>.*)/g

  const matches = re.exec(uri)
  let username

  try {
    const { pass, url, user, user2 } = matches.groups

    username = user || user2

    if (username) username = username.replace(':', '')

    if (username && pass && uri)
      return `mongodb://${supressUsername ? 'USERNAME' : username}:${supressPassword ? 'PASSWORD' : pass}@${url}`
    else if (username && uri)
      return `mongodb://${supressUsername ? 'USERNAME' : username}@${url}`
    else if (url)
      return `mongodb://${url}`
    return uri
  } catch (err) {
    throw new Error(`Malformed Mongo URI [${uri}]`)
  }
}

module.exports = mongoUriSafeLog
