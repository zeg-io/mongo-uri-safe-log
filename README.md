# Mongo URI Safe Log

This library is inteded for use within node.  Specifically it is useful for safely logging mongodb URIs, which is useful for debugging.  It allows you to easily remove username and password from the log output.

You can override default behavior to hide more or less information

| mongoUriSafeLog(uri, supressUser, supressPass) | Output |
| ----|----|
| mongoUriSafeLog(uri) | `mongodb://actual-username:PASSWORD@...`|
| mongoUriSafeLog(uri, true) | `mongodb://USERNAME:PASSWORD@...`| 
| mongoUriSafeLog(uri, true, false) | `mongodb://USERNAME:actual-password@...`|

#### Example

```js
const mongoUriSafeLog = require('mongo-uri-safe-log')

const mongoURI = 'mongodb://service-account:super-secret-password@db.somedomain.com:27017/collection?readPreference=primary&authSource=admin'

console.log(`Connecting to: ${mongoUriSafeLog(mongoURI)})
```

Output:

```sh
# mongoUriSafeLog(mongoURI)

mongodb://service-account:PASSWORD@db.somedomain.com:27017/collection?readPreference=primary&authSource=admin

# mongoUriSafeLog(mongoURI, true)

mongodb://USERNAME:PASSWORD@db.somedomain.com:27017/collection?readPreference=primary&authSource=admin

# mongoUriSafeLog(mongoURI, false, false)

mongodb://service-account:kdns673#8=@db.somedomain.com:27017/collection?readPreference=primary&authSource=admin
```

